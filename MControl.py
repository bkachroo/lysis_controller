# Proportional Temperature Control
# by: Bhareth Kachroo
# based on work by: Kashish Verma
# Update: 2018-05-25

#importing all requirements
import sys
import csv
import math
import os.path
import time
from time import sleep
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.ADC as ADC

ADC.setup()

# CONSTANTS ------------------------

pwm_frequency = 10000 # Hz
mix_frequency = 0.5 # Hz
coil_volts = 12 # V
coil_resistance = 2.5 # Ohms
run_time = 20 # sec
duty_cycle = 0.40 # fraction
coil_current = 2 # A

# SYSTEM VARIABLES -----------------
# Output pins don't directly control the system
# variables, so conversions are required.

def current_to_cycle (on_current):
    cycle = coil_resistance * on_current / coil_volts
    return cycle
    
# MAIN LOOP ------------------------------

def main_loop (run_time):
    start_time = time.time()

    while (total_time(start_time) < run_time):
        # run for the specified time
        pwm(current_to_cycle(coil_current)*100, pwm_frequency)
	print "cycle: ", current_to_cycle(coil_current)*100
        wait_on()
	print total_time(start_time)
        stop_pwm()
        wait_off()
	print "\n", total_time(start_time), "\n"

    # when loop finishes clean
    cleanup()
    print "\n Finished -------- \n"

# TIMING -------------------------------------

def wait_on():
    # Wait for the period defined by mix frequency
    sleep(duty_cycle/mix_frequency)

def wait_off():
    # Waiti for off portion of cycle
    sleep( (1-duty_cycle) / mix_frequency)

def total_time(start_time):
    # Time since control started
    return time.time() - start_time

# READING INPUTS PINS ------------------------
# Pins for interfacing with testing circuit

#Assigning pins for sensors
sensor = "P9_33"
heat_sink = "P9_35"
an3 = "P9_37"

#Assigning pin for PWM output
pwmPin = "P9_16"

#Assigning pin for H-bridge
In1 = "P9_11"

#Assigning pin for safety
safety = "P8_17"

#Setting up H-bridge Heat/Cool pins
GPIO.setup(In1, GPIO.OUT)
GPIO.output(In1, GPIO.LOW)

#Safety up safety pin
GPIO.setup(safety, GPIO.OUT)
GPIO.output(safety, GPIO.LOW)

# WRITING OUTPUT PINS ------------------------

def pwm(duty_cycle, frequency):
    PWM.start(pwmPin, duty_cycle, frequency)

def stop_pwm():
    PWM.stop(pwmPin)

def cleanup():
    GPIO.cleanup()
    PWM.cleanup()

# TEST ---------------------------------------

# RUN PROGRAM --------------------------------

main_loop(run_time)








