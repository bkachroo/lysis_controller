# Proportional Temperature Control
# by: Bhareth Kachroo
# based on work by: Kashish Verma
# Update: 2018-05-25

#importing all requirements
import sys
import csv
import math
import os.path
import time
from time import sleep
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.ADC as ADC

ADC.setup()

# CONSTANTS ------------------------

pwm_frequency = 10000 # Hz
poll_frequency = 0.5 # Hz
heater_volts = 12 # V
heater_resistance = 10 # Ohms
proportional_gain = 0.5 # W / C
T_set = 0 # degrees C
zero_out = 4 # W
time_to_hold = 60 # sec
hold_range = 5 # degrees C

# CONTROL MATH ---------------------

'''
Inputs: error (T), gain, zero error output (T)
Outputs: proportional control output (W)
'''
def proportional_control (error, gain, output_0):
    output = gain * error + output_0
    # Note control output is system input
    return output

# SYSTEM VARIABLES -----------------
# Output pins don't directly control the system
# variables, so conversions are required.

'''
Inputs: control output (W), heater voltage (V),
heater resistance (Ohms)
Outputs: pwm duty cycle (%)
'''
def p_to_c (p, v, R):
    c = (p * R / v**2 ) * 100
    return c

'''
Inputs: pwm duty cycle (%)
Outputs: clipped pwm duty cycle (%)
'''
def clip_c (c) :
    # c has to be within physical limits
    if c > 100:
        return 100
    elif c < 0:
        return 0
    else:
        return c

'''
Inputs: fluid temp (C), set temp (C)
Outputs: error temp (C)
'''
def Tf_to_error (T_f, T_set):
    error = T_set - T_f
    return error

'''
Inputs: sensor temp (C)
Outputs: fluid temp (C)
'''
def Tr_to_Tf (T_r):
    # Relationship between sensor temp and
    # fluid temp based on experiment
    T_f = T_r # Currently unknown
    return T_f

# CONTROL LOOP ------------------------------

'''
Inputs: sensor temp (C)
Outputs: pwm duty cycle (%)
'''
def control_step (T_r, T_set):
    T_f = Tr_to_Tf(T_r)

    error = Tf_to_error(T_f, T_set)

    p = proportional_control(error,
                             proportional_gain, zero_out)

    c = clip_c( p_to_c(p, heater_volts, heater_resistance) )

    return c

def main_loop(set_temp, time_to_hold):
    # initial variables
    start_time = time.time()
    hold = False # switching variable
    hold_time = 0

    while(not hold or held_time(hold_time) < time_to_hold):
        # input variables
        sensor_temp = read_sensor_temp()
        sink_temp = read_sink_temp()
        current_time = total_time(start_time)
        # check if set_temp is reached
        if not hold and abs(sensor_temp - set_temp) < hold_range:
            # if so start hold timing
            hold = True
            hold_time = time.time()
        # control
        cycle = control_step(sensor_temp, set_temp)
        # outputs
        #pwm(cycle, pwm_frequency)
        # write data
        write_data(current_time, sensor_temp, sink_temp, set_temp, cycle)
        # print everything
        print_data(current_time, sensor_temp, sink_temp, set_temp, cycle)
        # polling delay
        wait()
    # loop finishes
    stop_pwm()
    cleanup()
    print "\n Finished --------\n"

# TIMING -------------------------------------

def total_time(start_time):
    # Time since control started
    return time.time() - start_time

def held_time(start_hold):
    # Time since reaching set_temp
    return time.time() - start_hold

def wait():
    # Wait for the period defined by polling frequency
    sleep(1/poll_frequency)

# WRITING DATA -------------------------------

def print_data(time, sensor_temp, sink_temp, set_temp, duty_cycle):   
    print " "
    print "T(*C): ",sensor_temp, "HS(*C): ", sink_temp
    print "t(s): ", time
    print "DC: ", duty_cycle
    print " "

def write_data(time, sensor_temp, sink_temp, set_temp, duty_cycle):
    csvfile.write(str(time)+","+str(sensor_temp)+","
                  +str(sink_temp)+","+str(set_temp)+","+str(duty_cycle)+"\n")

# READING INPUTS PINS ------------------------
# Pins for interfacing with testing circuit

#Assigning pins for sensors
sensor = "P9_33"
heat_sink = "P9_35"
an3 = "P9_37"

#Assigning pin for PWM output
pwmPin = "P8_13"

#Assigning pin for H-bridge
In1 = "P9_11"

#Assigning pin for safety
safety = "P8_17"

#Setting up H-bridge Heat/Cool pins
GPIO.setup(In1, GPIO.OUT)
GPIO.output(In1, GPIO.LOW)

#Safety up safety pin
GPIO.setup(safety, GPIO.OUT)
GPIO.output(safety, GPIO.LOW)

'''
by: KV
Inputs: pin location
Outputs: sensor temp (C)
'''
def pin_to_Tr(pin):
    # Assuming the same circuit
    potVal=ADC.read_raw(pin)
    Vin = (1.8*potVal)/(4096*13)
    V1 = 1.8 - Vin
    I = V1/2000
    sensorR = Vin/I
    Temp = (2.6124*sensorR) - 261.71
    return Temp

'''
Inputs: N/A
Outputs: sensor temp(C)
'''
def read_sensor_temp():
    return pin_to_Tr(sensor)

'''
Inputs: N/A
Outputs: heat sink temp (C)
'''
def read_sink_temp():
    return pin_to_Tr(heat_sink)

# WRITING OUTPUT PINS ------------------------

def pwm(duty_cycle, frequency):
    PWM.start(pwmPin, duty_cycle, frequency)

def stop_pwm():
    PWM.stop(pwmPin)

def cleanup():
    GPIO.cleanup()
    PWM.cleanup()

# TEST ---------------------------------------

# USER INPUT ---------------------------------

#Ask the user to specify the name of the desired output file
print " "
out_file = raw_input("Enter the name of the .csv file you want to output your results to (format: example.csv): ")

#When the output.csv file already exists - asking the user to either change the name or overwrite existing file
if os.path.exists(out_file)==True:
        print " "
        print ".csv File with the same name already exists"
        print " "
        nextstep=raw_input("Do you want to continue and overwirte the file (Answer: y/n): ")

        if nextstep=='y' or nextstep =='Y' or nextstep == 'Yes' or nextstep=='yes' or nextstep=='YES':
                pass
        else:
                print " "
                out_file = raw_input("Please re-enter the new name of the .csv file you want to output your results to (format: example.csv): ")

#Opening our csv file to write
csvfile = open(out_file, 'wb')

#Writing onto the csv file
csvfile.write("P\n")
row=str(proportional_gain) + "\n"
csvfile.write(row)
csvfile.write("Begin Logging \n")
csvfile.write(" \n")
csvfile.write("Time(s), Sensor Temp(*C), Heat Sink Temp(*C), Set Temp(*C), Duty Cycle(%)\n")

# RUN PROGRAM --------------------------------

main_loop(T_set, time_to_hold)








